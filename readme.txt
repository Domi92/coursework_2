To see the web-app "MusicLab" follow the following instructions:

1. Open the livenux environment
2. On Windows open a ssh client like putty, on linux or mac you don't need a
ssh client. Just open the command line.
3. ssh into livenux.
    Host Name: tc@localhost
    password: foo
4. Navigate to coursework_2/src/
5. Type: 
	$python musiclab.py
6. If an error occurs that the module "bcrypt" could not be found, you need to install it first. To do that navigate to the folder libraries and type in the following commands: 	
	$sudo su
	$easy_install py_bcrypt-o.4-py2.7-linux-i686
	$exit
Now try it again with: 
	$python musiclab.py. 
It should work now!		
7. Open your favourite web browser and type "localhost:5000" into your address
line.
8. Sign in with your e-mail, choose a password, log in and Enjoy the web app




