
import ConfigParser
import bcrypt
import os
import sqlite3
import logging
from logging.handlers import RotatingFileHandler
from functools import wraps
from flask import Flask, abort, send_from_directory,  render_template, redirect, url_for, request, session, g
from werkzeug.utils import secure_filename

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'mp3'])

app = Flask(__name__)

db_location = 'var/musiclab.db'

app.secret_key = 'mysecretkey'

def allowed_file(filename):
  return '.' in filename and  filename.rsplit('.',1)[1] in ALLOWED_EXTENSIONS

def get_db():
  db = getattr(g, 'db', None)
  if db is None:
    db = sqlite3.connect(db_location)
    g.db = db
    db.row_factory = sqlite3.Row
  return db

@app.teardown_appcontext
def close_db_connection(exception):
  db = getattr(g, 'db', None)
  if db is not None:
    db.close()

def init_db():
  with app.app_context():
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
      db.cursor().executescript(f.read())
    db.commit()

def query_db(query, args=(), one=False):
	cur = get_db().execute(query, args)
	rv = cur.fetchall()
	cur.close()
	return (rv[0] if rv else None) if one else rv

def insert_user (name, surname, pw, mail):
  db = get_db()
  db.cursor().execute('INSERT INTO users (name,surname,pw,mail)VALUES(?,?,?,?)',(name,surname,pw,mail))
  db.commit()
  userCount = query_db('SELECT count(*) AS count FROM users', one=True)
  app.logger.info('The app has '+str(userCount['count'])+' user now :)')
  
def insert_post(content, file):
  userId = session['currend_user_id']
  db = get_db()
  db.cursor().execute('INSERT INTO posts (content, posted, audio)  VALUES(?,?,?)',(content, userId, file))
  db.commit()

def insert_comment(content, post):
  userId = session['currend_user_id']
  db = get_db()
  db.cursor().execute('INSERT INTO comments (content, post, commented)  VALUES(?,?,?)',(content, post, userId))
  db.commit()  

def show_comments(post_id):
  db = get_db()
  cur = db.execute('SELECT * FROM comments WHERE cId = (?) ORDER BY id desc',(post_id))
  comments = cur.fetchall()
  return render_template('comments.html', comments = comments)

def get_img():
  userId = session['currend_user_id']
  user = query_db('SELECT img FROM users WHERE userId = ?',[userId], one=True)
  img = user['img'] 
  return img

def check_auth(email, password):
  completion = False
  for user in query_db('SELECT pw, mail FROM users'):
    dbMail = user['mail']
    dbPass = user['pw']
    if dbMail == email:
      completion = check_password(dbPass, password)
  return completion

def check_password(hashedPw, userPw):
  return hashedPw == bcrypt.hashpw(userPw.encode('utf-8'), hashedPw)

def save_img_to_db(filename):
  userId = session['currend_user_id']
  db = get_db()
  db.cursor().execute('UPDATE users SET img = ? WHERE userId = ?',(filename, userId))
  db.commit()

def requires_login(f):
  @wraps(f)
  def decorated(*args, **kwargs):
    status = session.get('logged_in', False)
    if not status:
      return redirect(url_for('.root'))
    return f(*args, **kwargs)
  return decorated

@app.errorhandler(404)
def page_not_found(error):
  return render_template('error.html'), 404

@app.route('/')
def root():
  return render_template('welcome.html')

@app.route('/login/', methods = ['GET', 'POST'])
def login():
  if request.method == 'POST':
    mail = request.form['username']
    pw = request.form['psw']
    user = query_db('SELECT * FROM users WHERE mail = ? ',[mail], one=True)
    if check_auth(mail, pw):
      session['logged_in'] = True
      session['currend_user_id'] = user['userId']
      app.logger.info(user['name'] + " " + user['surname'] + " UserId = " + str(user['userId']) + " has logged in")
      return redirect(url_for('.home'))
  return render_template('login.html')

@app.route('/logout/')
def logout():
  session['logged_in'] = False
  id = session['currend_user_id']
  user = query_db('SELECT * FROM users WHERE userId = ?', [id], one=True)
  app.logger.info(user['name'] + " " + user['surname'] + " UserId = " + str(user['userId']) + " has logged out")
  return redirect(url_for('.root'))

@app.route('/signin/', methods = ['GET', 'POST'])
def signin():
  if request.method == 'POST':
    name = request.form['name']
    surname = request.form['surname']
    mail = request.form['mail']
    pw = request.form['signin-pwd']
    hashedPw = bcrypt.hashpw(pw, bcrypt.gensalt())
    insert_user(name,surname,hashedPw, mail)
    app.logger.info('New user signed in Name: '+name+' '+surname) 
    return redirect(url_for('.login'))
  return render_template('signin.html')

@app.route('/home/', methods = ['GET', 'POST'])
@requires_login
def home():
  if request.method == 'POST':
    content = request.form['message']
    if 'postfile' not in request.files:
      return redirect(request.url)
    file = request.files['postfile']
    if file.filename == '':
      return redirect(request.url)
    if file and allowed_file(file.filename):
      filename = secure_filename(file.filename)
      file.save(os.path.join(app.config['upload_folder'], filename))
      file_url = 'uploads/' + filename
      insert_post(content, file_url)
  id = session['currend_user_id']
  posts = query_db('SELECT * FROM posts ORDER BY postId desc')
  comments  = query_db('SELECT c.content, post, cId  FROM comments c, posts WHERE post = postId ORDER BY cId desc')
  commentcount = query_db('SELECT count(*) AS comments, post FROM comments c, posts WHERE post = postId GROUP BY post')
  postImg = query_db('SELECT img, postId FROM users, posts WHERE userId = posted')
  cImgs = query_db('SELECT img, cId FROM users, comments WHERE commented =userId')
  name  = query_db('SELECT name, surname FROM users WHERE userId = ?', [id],one=True)
  cNames = query_db('SELECT name, surname, cId FROM users, comments WHERE commented = userId')
  files = query_db('SELECT audio, postId FROM users, posts WHERE userId = posted')
  return render_template('home.html', posts=posts, name=name, comments =comments, commentcount=commentcount,img=get_img(),postImg=postImg,cImgs=cImgs, cNames = cNames, files=files)

@app.route('/mySite/')
@requires_login
def mySite():
  id = session['currend_user_id']
  posts = query_db('SELECT * FROM posts WHERE posted = ? ORDER BY postId desc',[id])
  comments  = query_db('SELECT c.content, post, cId  FROM comments c, posts WHERE post = postId ORDER BY cId desc')
  commentcount = query_db('SELECT count(*) AS comments, post FROM comments c, posts WHERE post = postId GROUP BY post')
  postImg = query_db('SELECT img, postId FROM users, posts WHERE userId = posted')
  cImgs = query_db('SELECT img, cId FROM users, comments WHERE commented =userId')
  name  = query_db('SELECT name, surname FROM users WHERE userId = ?', [id],one=True)
  cNames = query_db('SELECT name, surname, cId FROM users, comments WHERE commented = userId')
  files = query_db('SELECT audio, postId FROM users, posts WHERE userId = posted')
  return render_template('mySite.html',posts=posts,comments=comments,commentcount=commentcount,postImg=postImg,cImgs=cImgs,name=name,cNames=cNames,img=get_img(),files=files)

@app.route('/comment/<postId>/', methods = ['GET', 'POST'])
def comment(postId=None):
  if request.method == 'POST':
    comment = request.form['comment']
    insert_comment(comment, postId)
    return redirect(url_for('.home'))


@app.route('/upload_img/<fromSite>', methods =['GET', 'POST'])
def upload_img(fromSite = None):
  if request.method == 'POST':
    if 'new-img' not in request.files:
      return redirect(reques.url)
    file = request.files['new-img']
    if file.filename == '':
      return redirect(request.url)
    if file and allowed_file(file.filename):
      filename = secure_filename(file.filename)
      file.save(os.path.join(app.config['upload_folder'], filename))
      file_url = 'uploads/' + filename
      save_img_to_db(file_url)
  if fromSite == 'home':
    return redirect(url_for('.home'))
  return redirect(url_for('.mySite'))

def init(app):
  config = ConfigParser.ConfigParser()
  try:
    config_location = "etc/defaults.cfg"
    config.read(config_location)

    app.config['DEBUG'] = config.get("config", "debug")
    app.config['ip_address'] = config.get("config", "ip_address")
    app.config['port'] = config.get("config", "port")
    app.config['url'] = config.get("config", "url")
    app.config['upload_folder'] = config.get("config", "upload_folder")

    app.config['log_file'] = config.get("logging", "name")
    app.config['log_location'] = config.get("logging", "location")
    app.config['log_level'] = config.get("logging", "level")
  except:
    print "Could not read config from: ", config_location

def logs(app):
  log_pathname = app.config['log_location'] + app.config['log_file']
  file_handler = RotatingFileHandler(log_pathname, maxBytes=1024*1042*10, backupCount=1024)
  file_handler.setLevel( app.config['log_level'] )
  formatter = logging.Formatter("%(levelname)s | %(asctime)s | %(module)s | %(funcName)s | %(message)s")
  file_handler.setFormatter(formatter)
  app.logger.setLevel(app.config['log_level'])
  app.logger.addHandler(file_handler)

if __name__ == '__main__':
  init(app)
  logs(app)
  app.run(
    host = app.config['ip_address'],
    port = int(app.config['port']))
