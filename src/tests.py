import unittest
import musiclab

#ensure root page is displayed correctly
class TestRoot(unittest.TestCase):
  def test_root(self):
    self.app = musiclab.app.test_client()
    out = self.app.get('/')
    assert 'Welcome to MusicLab' in out.data	

#check log in
class TestLoginOut(unittest.TestCase):
  def login(self, username, psw):
    self.app = musiclab.app.test_client()
    return self.app.post('/login/', data=dict(
        username=username,
        psw=psw
    ), follow_redirects=True)

  def logout(self):
    return self.app.get('/logout/', follow_redirects=True)

  def test_login_logout(self):
    #right login
		out = self.login('domiweiger@mail.de', 'domi')
		assert 'Upload your new work here!' in out.data
    #logout
		out = self.logout()
		assert 'Welcome to MusicLab' in out.data
    #wrong username
		out = self.login('adminx', 'domi')
		assert 'Email:' in out.data
    #wrong password
		out = self.login('domiweiger@mail.de', 'defaultx')
		assert 'Email:' in out.data

if __name__ == "__main__":
  unittest.main()
