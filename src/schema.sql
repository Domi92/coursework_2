DROP TABLE if EXISTS users;
CREATE TABLE users (
	userId integer PRIMARY KEY autoincrement,
	name text,
	surname text,
	pw text,
	mail text,
	img text default 'uploads/platzhalter.jpg' 
);

DROP TABLE if EXISTS posts;
CREATE TABLE posts (
	postId integer PRIMARY KEY autoincrement,
	content text,
	audio text,
	posted integer,
	FOREIGN KEY (posted) REFERENCES users(userId)
);

DROP TABLE if EXISTS comments;
CREATE TABLE comments (
	cId integer PRIMARY KEY autoincrement,
	content text,
	post integer,
	commented integer,
	FOREIGN KEY (post) REFERENCES post(postId),
	FOREIGN KEY (commented) REFERENCES users(userId)
);



