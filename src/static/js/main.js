$(document).ready(function(){
    
	//show/hide comments
	var commentHidden = true;
	
	$('.glyphicon-comment').each(function(index){
		$(this).attr("forcomment", index);
	})
	$('.comment-wrapper').each(function(index){
		$(this).attr("refcomment", index);
	})
	$('.glyphicon-comment').on('click', function() {
			var refTo = $(this).attr('forcomment');

			$('.comment-wrapper').each(function(index){
			
			var ref = $(this).attr('refcomment');
				if(ref == refTo){
					if (commentHidden){
						$(this).toggleClass('active');
						commentHidden = false;
					}
					else $(this).toggleClass('active');
				}
		});
	})
	
	// helper for fileupload
	
	$("#new-img").on('change', function() {
		$("#upload-form").submit();
	});
	
	// special hover for img-upload
	
	$('#upload-img').on('mouseover', function() {
		$('.hover-upload').css('display', 'block')
	});
	
	$('#upload-img').on('mouseout', function() {
		$('.hover-upload').css('display', 'none')
	});
	
	//getting filename and add it to Audio upload btn
	
	$('#postfile').on('change', function(){
		if ($('#postfile').val().split('\\').pop()) {
			var filename = $('#postfile').val().split('\\').pop();
			$('.upload-btn').text(filename);
		}
	});
	
	//sign in form validation
	$('#signin-form').on('submit', function() {
	
		if ($('#name').val() == '') {
			$('#error-name').css('display', 'inline-block');
			missing = true;
		}
		
		else {
			$('#error-name').css('display', 'none');
			missing = false;
		}
		
		if ($('#surname').val() == '') {
			$('#error-surname').css('display', 'inline-block');
			missing = true;
		}
		
		else {
			$('#error-surname').css('display', 'none');
			missing = false;
		}
		
		if ($('#mail').val() == '') {
			$('#error-mail').css('display', 'inline-block');
			missing = true;
		}
		
		else {
			$('#error-mail').css('display', 'none');
			missing = false;
		}
		
		if ($('#pw').val() == '') {
			$('#error-password').css('display', 'inline-block');
			missing = true;
		}
		
		else if($('#pw').val() != $('#check-pw').val()){
				$('#error-password-check').css('display', 'inline-block');
				$('#error-password').css('display', 'none');
				missing = true;
			}
			else {
				$('#error-password-check').css('display', 'none');
				$('#error-password').css('display', 'none');
				missing = false;
			}
			
		
		if (missing) return false;
		else return true;
	});
	
	//Upload validation
	$('#upload').on('submit', function() {
		var extension = $('#postfile').val().substr( ($('#postfile').val().lastIndexOf('.') +1) );
		if($('#postfile').val() == '' || extension != 'mp3'){
			$('#upload-error').css('display', 'inline-block');
			return false;
		}
		else return true;
	});
	
	//init functions
});